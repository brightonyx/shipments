<?php
/**
 * Bootstrap for the entire project
 *
 * @author Paul Brighton
 * @date August 2021
 * @since 1.0
 *
 */

// Set strict types requirement
declare(strict_types=1);
// Define some constants
define('ROOT_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('VIEW_PATH', ROOT_PATH . 'src' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);

// Make sure the composer libraries are loaded
require ROOT_PATH . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use Controllers\ShippingController;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

// Register the error handler
$whoops = new Run();

// Read the current URI path
$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

// Send default headers
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3600');

// If this is a landing page, render HTML
if ($uri === '/') {
	// Set the error reporting handler to HTML
	$whoops->pushHandler(new PrettyPageHandler());
	$whoops->register();
	header('Content-Type: text/html; charset=UTF-8');
	(new ShippingController())->index();
} else {
	// Otherwise, proceed as JSON REST listener
	header('Content-Type: application/json; charset=UTF-8');
	header('Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE');
	header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

	// Set the error reporting handler to JSON
	$whoops->pushHandler(new JsonResponseHandler());
	$whoops->register();

	$uri = explode('/', $uri);
	// Only allow /shipping resource
	if ($uri[1] !== 'shipping') {
		header('HTTP/1.1 404 Not Found');
		exit();
	}

	// Get the HTTP method to determine the action
	$method = $_SERVER['REQUEST_METHOD'];

	// Pass the request method and payload to the ShippingController, and process the HTTP request
	$controller = new ShippingController();
	$controller->process_request($method, $uri);
}
