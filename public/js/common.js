window.addEventListener('load', function() {
	// Wrapper for making AJAX calls
	$.fn.post = function (endpoint, callback) {
		// Allow only the forms to use this function
		if (this.is('form')) {
			// Process the API request
			$.ajax({
				url: endpoint,
				type: 'POST',
				contentType: 'application/json',
				data: $(this).serializeJSON(),
				fail: function () {
					showError();
				},
				error: function (xhr, textStatus, errorThrown) {
					// Show the server error. Fallback to the default AJAX exception
					let error = errorThrown;
					if (xhr.response) {
						let jsonResponse = JSON.parse(xhr.response);
						if ('error' in jsonResponse) {
							// Capture WHOOPS and BE errors
							error = typeof jsonResponse.error == 'string' ? jsonResponse.error : jsonResponse.error.message;
						}
					}
					showError(error);
				},
				success: function (response) {
					if (response) {
						if (response.success === true) {
							// Run the callback if defined
							if (callback) {
								callback(response);
							}
							if (response.message) {
								showSuccess(response.message);
							}
						} else {
							renderError(response.error);
						}
					} else {
						showError('No data received from the server...');
					}
				}
			});
		} else {
			showError('post() method can only be applied to forms');
		}
		return this;
	};

	// Creates a proper JSON object from form data
	$.fn.serializeJSON = function () {
		let jsonObject = {};
		Zepto.map($(this).serializeArray(), function (n, i) {
			jsonObject[n['name']] = n['value'];
		});
		return JSON.stringify(jsonObject);
	};

	// Get the alert box and set is as a global variable
	let alert = $('#alert');

	/**
	 * Shows an error in the 'alert' container
	 *
	 * @param message
	 */
	function showError(message = 'Form submit error. Please try again') {
		// Remove any success classes and add an error class
		alert.removeClass('tertiary').addClass('secondary');
		// Show the text and apply the pre-defined transitions
		updateAlertContainer(message);
	}

	/**
	 * Shows a success message in the 'alert' container
	 *
	 * @param message
	 */
	function showSuccess(message = 'Form submitted successfully') {
		// Remove any error classes and add a success class
		alert.removeClass('secondary').addClass('tertiary');
		// Show the text and apply the pre-defined transitions
		updateAlertContainer(message);
	}

	/**
	 * Internal method that fades in and out an actual element
	 *
	 * @param message
	 */
	function updateAlertContainer(message) {
		// Inject the message text and trigger a transition - show slow
		alert.html(message).show('slow');
		// Remove the message after 5 seconds
		setTimeout(function () {
			// Set the transition to 3 seconds
			alert.hide();
		}, 5000);
	}
}, false);