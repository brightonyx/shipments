# Remote Technical Developer Task (v2.2) #

Create an application that allows users to enter shipment information and retrieve shipping rates
from Printful's API. In order to speed up the process, create a caching mechanism and use it to
store API results for faster retrieval when the same shipment information is provided.

### Requirements: ###

* Create a class that connects to Printful's Shipping Rate API
[https://www.printful.com/docs/shipping](https://www.printful.com/docs/shipping) and retrieves the list of available shipping
options using values submitted by the user.
API authorization docs: https://www.printful.com/docs
API Key: 77qn9aax-qrrm-idki:lnh0-fm2nhmp0yca7
* Create a persistent file caching mechanism which implements given [interface](https://github.com/printful/homework-task/blob/main/app/interfaces/CacheInterface.php)
* API results must be cached using the previously implemented cache.
* Cache interface must be constructor-injected into the service.
* Use Guzzle library for API requests.
* Use Composer for autoloading classes.
* Write clean, reusable and well formatted code.
* Avoid using frameworks.
* Avoid using any other packages except Guzzle, PHPUnit

### Testing scenarios: ###

Test your application with these scenarios:
1. User enters shipment information:
   * Address = 19749 Dearborn St, Chatsworth, CA, 91311, US
   * Shipment items:
      - Item variant id = 2, quantity = 1
      - Item variant id = 202, quantity = 5,
2. User enters shipment information:
   * Address = 11025 Westlake Dr, Charlotte, NC, 28273, US
   * Shipment items:
      - Item variant id = 7679, quantity = 1
      - Item variant id = 202, quantity = 1

### Notes: ###

* API key in authorization header should be base64 encoded (as stated in the API docs)
* We have created a project template for easier testing and setup. You can modify these
files or just skip this template and build everything yourself: https://github.com/printful/homework-task
