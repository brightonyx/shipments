<?php
/** Describing some of the view variables */
/** @var ?string $results */
/** @var array $recipient */
/** @var array $items */
?>
<!doctype html>
<html lang="">
<head>
	<meta charset="utf-8">
	<title>Printful Code Challenge</title>
	<meta name="description" content="Homework task v2.2">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="https://www.printful.com/favicon.png">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mini.css/3.0.1/mini-default.min.css">
	<style>
		.container {
			width: 720px;
		}
		input {
			width: 99%;
		}
		table {
			overflow: hidden !important;
		}
		table th, table td {
			flex: 1 0 auto !important;
		}
	</style>
</head>
<body>
	<div class="container">
		<h1>Shipment rates</h1>
		<form id="shipping-rates-form" method="POST">
			<fieldset>
				<legend>Shipment information</legend>
				<div>
					<input placeholder="Address" name="recipient[address1]" value="<?= $recipient['address1'] ?? null ?>">
				</div>
				<div>
					<input placeholder="City" name="recipient[city]" value="<?= $recipient['city'] ?? null ?>">
				</div>
				<div>
					<input placeholder="State code" name="recipient[state_code]" value="<?= $recipient['state_code'] ?? null ?>" required>
				</div>
				<div>
					<input placeholder="ZIP code" name="recipient[zip]" value="<?= $recipient['zip'] ?? null ?>">
				</div>
				<div>
					<input placeholder="Country code" name="recipient[country_code]" value="<?= $recipient['country_code'] ?? null ?>" required>
				</div>
			</fieldset>
			<br>
			<fieldset>
				<legend>Order items</legend>
				<fieldset>
					<legend>1. item</legend>
					<div>
						<input placeholder="Variant ID" name="items[0][variant_id]" value="<?= $items[0]['variant_id'] ?? null ?>" required>
					</div>
					<div>
						<input placeholder="Quantity" name="items[0][quantity]" value="<?= $items[0]['quantity'] ?? null ?>" required>
					</div>
				</fieldset>
				<fieldset>
					<legend>2. item</legend>
					<div>
						<input placeholder="Variant ID" name="items[1][variant_id]" value="<?= $items[1]['variant_id'] ?? null ?>" required>
					</div>
					<div>
						<input placeholder="Quantity" name="items[1][quantity]" value="<?= $items[1]['quantity'] ?? null ?>" required>
					</div>
				</fieldset>
			</fieldset>
			<br>
			<div>
				<button id="submit-button" class="primary" type="submit">Get shipping rates</button>
			</div>
			<p>
				<mark class="tertiary" id="alert" style="display: none"></mark>
			</p>
		</form>
	</div>
	<div class="container">
		<br>
		<div id="results" style="display: none">
			<h1>Available Rates</h1>
			<div class="row"></div>
		</div>
	</div>

	<script type="application/javascript" src="https://zeptojs.com/zepto.min.js"></script>
	<script type="application/javascript" src="/js/common.js"></script>
	<script>
		Zepto(function($){
			$('#shipping-rates-form').on('submit', function (e) {
				// Disable any default form actions
				e.preventDefault();
				// Disable the submit button to avoid duplicate requests
				let button = $('#submit-button').attr('disabled', true);
				$(this).post('/shipping', function(response) {
					// Re-enable the button back
					button.removeAttr('disabled');
					// Generate the "cards" with the rates and description
					let cards = '';
					$.each(response.data.results, function (index, row) {
						cards += '<div class="card fluid"><div class="section dark">';
						cards += '<h4>' + row.id + ' $' + row.rate + ' ' + row.currency + '</h4>';
						cards += '<p>' + row.name + '</p></div></div>';
					});
					// Append the HTML back to the parent div
					$('#results .row').html(cards);
					$('#results').show();
				});
			});
		});
	</script>
</body>
</html>
