<?php
/**
 * Shipping controller that executes the logic for each endpoint
 *
 * @author Paul Brighton
 * @date August 2021
 * @since 1.0
 *
 */

namespace Controllers;

use Services\ShippingService;
use Services\ViewerService;
use Helpers\Util;

class ShippingController {

	/** @var string $view_name Name of the view to associate with this controller */
	private $view_name = 'shipping-form';

	/**
	 * Renders a basic landing page with a form
	 *
	 */
	public function index(): void {
		ViewerService::render($this->view_name, ['recipient' => [], 'items' => []]);
	}

	/**
	 * Reads the HTTP request method and performs a corresponding model action
	 * Currently only POST method is supported. We could extend the functionality by processing GET, PUT and DELETE as well
	 * 
	 * @param string $request_method
	 * @param array $uri the parsed URL path. Needed for RESTful endpoints which are not implemented yet
	 */
	public function process_request(string $request_method, array $uri): void {
		// Instantiate a service that we will delegate the task to
		$shipping_service = new ShippingService();
		// Get user input from a JSON payload
		$user_data = Util::get_form_input();
		switch ($request_method) {
			case 'PUT':
			case 'DELETE':
			case 'GET':
				Util::json_error('Method not supported');
				break;
			case 'POST':
				$shipping_service->get_rates($user_data);
				break;
			default:
				Util::json_error('Invalid request method');
				break;
		}
	}
}