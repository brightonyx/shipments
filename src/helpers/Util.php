<?php
/**
 * Utility class that includes various helper functions, wrappers and tools to aid the main app development
 *
 * @author Paul Brighton
 * @date August 2021
 * @since 1.0
 */

namespace Helpers;

use Entities\Response;

class Util {

	/**
	 * Prints the contents of an array of an object in a human-readable format
	 *
	 * @param object|array|string $entity The entity to print - could be object, array or string
	 * @param bool $return To return the formatted variable or output directly on the screen
	 * @return string
	 */
	public static function printr($entity, bool $return = false): string {
		$result = '<pre>' . print_r($entity, true) . '</pre>';
		if ($return) {
			return $result;
		}
		echo $result;
		return '';
	}

	/**
	 * Parses the JSON form input into a proper PHP array
	 *
	 * @return array
	 */
	public static function get_form_input(): array {
		$input = json_decode(file_get_contents('php://input'), true);
		$query = '';
		foreach ($input as $key => $value) {
			$query .= $key . '=' . $value . '&';
		}
		parse_str(rtrim($query, '&'), $data);
		return $data;
	}

	/**
	 * Acts as a wrapper to send json error response back to the client
	 *
	 * @param string $message Error message
	 * @param int $status HTTP status code
	 */
	public static function json_error(string $message, int $status = 400): void {
		$response          = new Response();
		$response->error   = $message;
		$response->success = false;
		self::send_json($response, $status);
	}

	/**
	 * Acts as a wrapper to send json success response back to the client
	 *
	 * @param string $message Success message
	 * @param int $status HTTP status code
	 */
	public static function json_success(string $message, int $status = 200): void {
		$response          = new Response();
		$response->message = $message;
		self::send_json($response, $status);
	}

	/**
	 * Acts as a wrapper to send json data response back to the client
	 *
	 * @param object|array $data
	 * @param string|null $message
	 * @param int $status HTTP status code
	 */
	public static function json_data($data, string $message = null, int $status = 200): void {
		$response          = new Response();
		$response->data    = $data;
		$response->message = $message;
		self::send_json($response, $status);
	}

	/**
	 * Main logic that sends off json response data and headers including a proper status code
	 *
	 * @param Response $response
	 * @param int $status
	 */
	public static function send_json(Response $response, int $status = 200): void {
		$status_map = [
			200 => 'HTTP/1.1 200 OK',
			400 => 'HTTP/1.1 400 Bad Request',
			422 => 'HTTP/1.1 422 Unprocessable Entity',
			401 => 'HTTP/1.1 401 Unauthorized',
			404 => 'HTTP/1.1 404 Not Found',
			500 => 'HTTP/1.1 500 Internal Server Error',
			503 => 'HTTP/1.1 500 Service Unavailable',
		];
		// Send the HTTP status code
		header($status_map[$status]);
		die(json_encode($response));
	}
}
