<?php
/**
 * Viewer service. Used for rendering source files and producing HTML
 *
 * @author Paul Brighton
 * @date August 2021
 * @since 1.0
 *
 */

namespace Services;

class ViewerService {

	/**
	 * Renders a specific view file, injecting variables to bind
	 * Currently expects the view file to contain the full HTML.
	 * In production of course, we would stitch header, body, and footer separately
	 *
	 * @param string $file_path Relative location of the view file
	 * @param array $variables List of parameters to pass down to the view
	 */
	public static function render(string $file_path, array $variables = []): void {
		ob_start();
		extract($variables, EXTR_OVERWRITE);
		include(VIEW_PATH . $file_path . '.php');
		echo ob_get_clean();
	}
}