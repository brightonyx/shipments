<?php
/**
 * Shipping service. Responsible for getting the rates
 *
 * @author Paul Brighton
 * @date August 2021
 * @since 1.0
 *
 */

namespace Services;

use GuzzleHttp\Exception\BadResponseException;
use Helpers\Util;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ShippingService {

	/** @var Client $client Local instance of Guzzle client */
	private $client;

	/** @var CachingService $cache Local instance of a caching service */
	private $cache;

	/** @var int CACHE_TTL default duration of a cached item */
	public const CACHE_TTL = 60;

	/**
	 * ShippingController constructor.
	 * This of course, will be moved up to a base controller that all controller must extend as this sets up global services
	 *
	 */
	public function __construct() {
		// Credentials are hard coded here for prototyping. Obviously this would come from a secure source or a config in production
		// This is an alternative way oif specifying basic auth. Guzzle has a native header spec so we don't need to do anything
		// $credentials = base64_encode('77qn9aax-qrrm-idki:lnh0-fm2nhmp0yca7');
		// $response = $this->client->get('url', ['headers' => ['Authorization' => 'Basic ' . $credentials]]);
		$this->client = new Client([
			'base_uri' => 'https://api.printful.com/',
			'timeout'  => 5.0,
			'verify'   => false,
			'headers'  => ['Content-Type' => 'application/json; charset=UTF-8'],
			'auth'     => ['77qn9aax-qrrm-idki', 'lnh0-fm2nhmp0yca7'],
		]);

		// Init the caching service
		$this->cache = new CachingService();
	}

	/**
	 * Retrieves the shipping rates from the API and caches the results
	 * This can also be done in a service but in an interest of time, we'll do it directly in a controller
	 *
	 * @param array $user_data
	 */
	public function get_rates(array $user_data = []): void {
		// Get a unique key from the user data
		$key = $this->cache->generate_key($user_data);
		// Check if the value is in cache
		if ($results = $this->cache->get($key)) {
			// Return the results as JSON
			Util::json_data(['results' => $results], 'Got the rates from cache');
		} else {
			// Make an API call
			try {
				$response = $this->client->post('shipping/rates', ['json' => $user_data]);
				if ($response->getStatusCode() === 200 and $body = (string)$response->getBody()) {
					$response = json_decode($body);
					// Cache the result
					$this->cache->set($key, $response->result, self::CACHE_TTL);
					// Return the results as JSON
					Util::json_data(['results' => $response->result]);
				} else {
					throw new BadResponseException('API error: ' . $response->getReasonPhrase());
				}
			} catch (GuzzleException $e) {
				//Util::json_error($e->getMessage());
				Util::json_data(['user_data' => $user_data], $e->getMessage());
			}
		}
	}
}