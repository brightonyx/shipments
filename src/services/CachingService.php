<?php
/**
 * Caching service. Used for data storage and retrieval from session on disk
 * This of course can and should be extended to support other cache drivers
 *
 * @author Paul Brighton
 * @date August 2021
 * @since 1.0
 *
 */

namespace Services;

use Interfaces\CacheInterface;

class CachingService implements CacheInterface {

	/**
	 * Generates an md5 hash from the data array which will be used as a cache key
	 *
	 * @param array $data
	 * @return string
	 */
	public function generate_key(array $data): string {
		// We will use native serialize method instead of json_encode as it is roughly 50% faster (on PHP8)
		return md5(serialize($data));
	}

	/** @inheritdoc */
	public function set(string $key, $value, int $duration) {
		// Generate the file path in the temp system directory
		$file_path = $this->get_file_path($key);
		// Create a cacheable item by storing a duration in it
		$item = [
			'duration' => time() + $duration,
			'payload'  => $value
		];
		// Write the serialized version of the cacheable item to a file path
		// By overwriting a file every time, we extend the lifetime of a cacheable item
		return file_put_contents($file_path, serialize($item));

		// NOTE: since the instructions did not specify "which" filesystem to use, we can easily use cookies instead
		// It would be much easier to deal with cache expiration as a duration logic is already built in
	}

	/** @inheritdoc */
	public function get(string $key) {
		// Get the file path where the cache is stored
		$file_path = $this->get_file_path($key);
		// If a file exists and is readable
		if (is_readable($file_path)) {
			// Read the deserialized contents
			$item = unserialize(file_get_contents($file_path), ['allowed_classes' => false]);
			// Make sure the cache item contains both duration and the payload
			if (isset($item['duration']) and isset($item['payload'])) {
				// If the cache item is still within the lifetime, return the payload
				if (time() <= (int)$item['duration']) {
					return $item['payload'];
				}
			}
		}
		// Otherwise, return null
		return null;
	}

	/**
	 * Gets a full path to a cached file
	 *
	 * @param string $key
	 * @return string
	 */
	private function get_file_path(string $key): string {
		// Get a system directory
		$directory = sys_get_temp_dir();
		// Return a file path in that directory with a given key
		return $directory . DIRECTORY_SEPARATOR . $key . '.tmp';
	}
}